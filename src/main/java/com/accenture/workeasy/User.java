package com.accenture.workeasy;

import java.util.Map;

public class User {
  private String domain;
  private String userName;
  private char[] password;
  private String wbse;
  private String role;
  private String month;
  private Integer year;
  private boolean isUSStyleDate;
  private Map<String, Map<String, String>> workHoursMap;
  
  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }
  
  public String getUserName() {
    return userName;
  }
  
  public void setUserName(String userName) {
    this.userName = userName;
  }
  
  public char[] getPassword() {
    return password;
  }
  
  public void setPassword(char[] password) {
    this.password = password;
  }
  
  public String getWbse() {
    return wbse;
  }
  
  public void setWbse(String wbse) {
    this.wbse = wbse;
  }
  
  public String getRole() {
	return role;
}

public void setRole(String role) {
	this.role = role;
}

public String getMonth() {
    return month;
  }
  
  public void setMonth(String month) {
    this.month = month;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

public boolean isUSStyleDate() {
	return isUSStyleDate;
}

public void setUSStyleDate(boolean isUSStyleDate) {
	this.isUSStyleDate = isUSStyleDate;
}

public Map<String, Map<String, String>> getWorkHoursMap() {
	return workHoursMap;
}

public void setWorkHoursMap(Map<String, Map<String, String>> workHoursMap) {
	this.workHoursMap = workHoursMap;
}
  
}
