package com.accenture.workeasy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.prefs.Preferences;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.accenture.workeasy.beans.Result;
import com.accenture.workeasy.constants.UserFields;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
 
public class Main extends Application {
  
 private Calendar now = Calendar.getInstance();
 
 private Preferences prefs;
 
  private final ObservableList<Month> monthList =
      FXCollections.observableArrayList(
      new Month("January", "1"),
      new Month("February", "2"),      
      new Month("March", "3"),
      new Month("April", "4"),
      new Month("May", "5"),
      new Month("June", "6"),
      new Month("July", "7"),
      new Month("August", "8"),
      new Month("September", "9"),
      new Month("October", "10"),
      new Month("November", "11"),
      new Month("December", "12"));
  
  
    @Override
    public void start(Stage primaryStage) {
      if(new File("profile").exists()){
    	profileCleanUp();
      }
      
      primaryStage.setTitle("Work Hours Analysis");
      
      if(prefs == null) {
    	  prefs = Preferences.userRoot().node(this.getClass().getName());
      }
      
      StackPane root = new StackPane();
      
            
      //Image img = new Image("images/default.gif");
      //ImageView imgView = new ImageView(img);
      //imgView.setVisible(false);
      
      GridPane grid = getMainGrid(new GridPane());
      
      try{
    	  primaryStage.getIcons().addAll(
        		  //new Image(Main.class.getResourceAsStream("/images/icons/favicon-16x16.png")),
        		  //new Image(Main.class.getResourceAsStream("/images/icons/favicon-96x96.png")),
        		 // new Image(Main.class.getResourceAsStream("/images/icons/favicon-32x32.png")),
        		  new Image(Main.class.getResourceAsStream("/images/icons/ms-icon-310x310.png"))
        		  //new Image(Main.class.getResourceAsStream("/images/icons/ms-icon-150x150.png"))
        		  /*new Image(Main.class.getResourceAsStream("/images/icons/ms-icon-70x70.png"))*/
    	  );  
      }catch(Exception e){
    	  e.printStackTrace();
      }
      
      
      Text scenetitle = new Text("Welcome");
      scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
      grid.add(scenetitle, 0, 0, 2, 1);
      

      Label domainName = new Label("Domain Name:");
      grid.add(domainName, 0, 1);

      TextField domainTextField = new TextField();
      domainTextField.setText(prefs.get(UserFields.DOMAIN.name(), "DIR"));
      grid.add(domainTextField, 1, 1);
      
      Label userName = new Label("User Name:");
      grid.add(userName, 0, 2);

      TextField userTextField = new TextField();
      userTextField.setText(prefs.get(UserFields.USERNAME.name(), ""));
      grid.add(userTextField, 1, 2);
      

      Label pw = new Label("Password:");
      grid.add(pw, 0, 3);

      PasswordField pwBox = new PasswordField();
      grid.add(pwBox, 1, 3);
     
      
      
      
      Label role = new Label("Role:");
      grid.add(role, 0, 4);
      
      HBox hbox = new HBox();
      final ToggleGroup group = new ToggleGroup();
      
      RadioButton rbDelegatee = new RadioButton("Delegate");
      rbDelegatee.setToggleGroup(group);
      rbDelegatee.setSelected(true);
      rbDelegatee.setUserData("Delegate");

      RadioButton rbSelf = new RadioButton("Self");
      rbSelf.setToggleGroup(group);
      rbSelf.setUserData("Self");
      
      hbox.getChildren().add(rbDelegatee);
      hbox.getChildren().add(rbSelf);
      
      hbox.setSpacing(20);
      
      grid.add(hbox, 1, 4);
      
      
      
      Label mon = new Label("Month:");
      grid.add(mon, 0, 6);

      ComboBox<Month> combobox = new ComboBox<>(monthList);
      combobox.getSelectionModel().select(now.get(Calendar.MONTH) - 1); 
      grid.add(combobox, 1, 6);
      
      Button btn = new Button("Sign in");
      HBox hbBtn = new HBox(10);
      hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
      hbBtn.getChildren().add(btn);
      grid.add(hbBtn, 1, 7);
      
      final Label actiontarget = new Label();
      grid.add(actiontarget, 1, 9);
      
      root.getChildren().addAll(grid);
      
      Scene scene = new Scene(root, 400, 350);
      primaryStage.setScene(scene);
      
      
      primaryStage.show();
      
      
      
      btn.setOnAction(new EventHandler<ActionEvent>() {
        
        @Override
        public void handle(ActionEvent e) {
        	
        	Platform.runLater(new Runnable() { 
                @Override
                public void run() {
                  Instant startInstant = Instant.now();
                	btn.setDisable(true);
                	actiontarget.setText("---working---");
                	//imgView.setVisible(true);
                    //grid.setStyle("-fx-opacity: 0.01;");
                      
                      final User user = setUserInfo(new User());
                      
                      storeUserInfo(user);
                      ObjectProperty<Result> resultProperty = new SimpleObjectProperty<>();
                      ExecutorService executorService = Executors.newSingleThreadExecutor();  
                      Supplier<Result> supplier = null;
                      
                      ObjectProperty<String> totalDurationProperty = new SimpleObjectProperty<>();
                      
                      WebDriver driver = setUpDriver();
                      
                      if("Delegate".equalsIgnoreCase(user.getRole())){
                    	  supplier = new MyTEDelegateSupplier(user, driver);
                      } else {
                    	  supplier = new MyTESupplier(user, driver);
                      }
                      // Future<User> future = executorService.submit(new MyTEAnalyser(user));
                      if(user != null){
                    	  
                      }
                      
                      
                      CompletableFuture.supplyAsync(supplier, executorService).thenAcceptAsync((Result resultUpdated) -> {
                        if(driver != null){
                          driver.quit();
                        }
                        /*if(resultUpdated.ge != null){
                          
                        }*/
                        if(resultUpdated != null && resultUpdated.isSuccess()) {
                          resultProperty.set(resultUpdated);
                          User user2 = resultUpdated.getUser();
                          user.setWorkHoursMap(user2.getWorkHoursMap());
                        }else{
                          resultProperty.set(resultUpdated);
                        }
                    	  
                          
                      }).thenRunAsync(() -> {
                          if(resultProperty.get() != null && resultProperty.get().isSuccess()){
                            String msg = "Operation completed successfully. Now, processing the result. \nPlease wait for a few more minutes.";  
                            actiontarget.setText(msg);
                            primaryStage.setWidth(600);
                            primaryStage.setHeight(400);
                          }else{
                            String msg = "Something went wrong. Please try again later or try doing manually.\nContact the support desk.";  
                            actiontarget.setText(msg);
                            primaryStage.setWidth(600);
                            primaryStage.setHeight(400);
                          }
                        	
                        }, new Executor() {
                            @Override
                            public void execute(Runnable command) {
                              Platform.runLater(command);
                            }
                          }).thenRunAsync(() -> {
                        	transferDataToFile(resultProperty.get());
                        }).thenRunAsync(() -> {
                          Instant endInstant = Instant.now();
                          Duration duration = Duration.between(startInstant , endInstant);
                          long time = duration.toMinutes();
                          String totalDuration = "";
                          if (time > 60) {
                            long hours = time / 60;
                            long minutes = time % 60;
                            totalDuration = String.format("%d hour %02d minutes", hours, minutes);
                          } else {
                            totalDuration = String.format("%02d minutes", time);
                          }
                          totalDurationProperty.setValue(totalDuration);
                        }).thenRunAsync(() -> {
                        	  actiontarget.setText("Done :)\nTotal time taken is " + totalDurationProperty.getValue());
                        }, new Executor() {
                            @Override
                            public void execute(Runnable command) {
                              Platform.runLater(command);
                            }
                          }).thenRunAsync(() -> {
                        	  profileCleanUp();
                        	  btn.setDisable(false);
                          });

                      
                      Runtime.getRuntime().addShutdownHook( 
                      		new Thread(
                      			new Runnable() {
                      				public void run() {
                      				  if(driver != null){
                      				    driver.quit();
                      				  }
                      					System.out.println("Shutdown Ran");
                      					profileCleanUp();
                      				}	
                      			}
                      		)
                      	);

                       
                }
            });
          
        	primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                	profileCleanUp();
                    Platform.exit();
                }
        	});
        }
        
        
        private WebDriver setUpDriver() {
          try {
            setSystemProps();
            return getWebDriver();
          } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
          
          return null;
        }
        
        /**
         * Set necessary system properties. i.e. Set driver location for chrome/ firefox/ IE etc for different OS like windows, linux, OSX etc.
         * @throws Exception 
         */
        private void setSystemProps() throws Exception {
          String driverPath = getDriverName();
          if(driverPath == null) throw new Exception("OS Unknown");
          File chromeDriver = getDriverFilePath(driverPath);
        System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
      }
        
        private String getDriverName() {
          OsCheck.OSType ostype=OsCheck.getOperatingSystemType();
          String driverPath = null;
          switch (ostype) {
              case Windows:
                driverPath = "execs/chromedriver.properties";
                break;
              case MacOS: 
                driverPath = "execs/chromedriver_mac";
                break;
              case Linux:
                boolean is64bit = System.getProperty("os.arch").indexOf("64") != -1; 
                if(is64bit) {
                  driverPath = "execs/chromedriver_linux_64";
                } else {
                  driverPath = "execs/chromedriver_linux_32";
                }
                break;
              case Other: 
                break;
          }
          
          return driverPath;
      }

      private File getDriverFilePath(String driverPath) throws IOException, FileNotFoundException {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(driverPath);
        File chromeDriver = new File("profile/" + driverPath);
        createDirs(chromeDriver);
        FileOutputStream outputStream = new FileOutputStream(chromeDriver);
        IOUtils.copy(inputStream, outputStream);
        outputStream.flush();
        outputStream.close();
        if(!chromeDriver.canExecute()){
          chromeDriver.setExecutable(true);
        }
        return chromeDriver;
      }
      
      /**
       * 
       * @return WebDriver - return webdriver of the browser
       * @throws URISyntaxException 
       * @throws IOException 
       */
    private WebDriver getWebDriver() throws URISyntaxException, IOException {
        ChromeOptions options = new ChromeOptions();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("execs/symantac.crx"); 
        File file = new File("profile/execs/symantac.crx");
        createDirs(file);
        OutputStream outputStream = new FileOutputStream(file, false);
        IOUtils.copy(inputStream, outputStream);
        outputStream.flush();
        outputStream.close();
        options.addExtensions(file);
        System.out.println(file.exists());
        
        File profileDir = new File("profile/chrome-test/");
        if(!profileDir.getParentFile().exists()){
          profileDir.mkdirs();
        }
        if(!profileDir.exists()){
          profileDir.mkdir();
        }
        System.out.println(profileDir.getAbsolutePath());
          options.addArguments("--user-data-dir="+profileDir.getAbsolutePath());
          options.addArguments("--start-maximized");
          WebDriver driver = new ChromeDriver(options);
          return driver;
    }

    private void createDirs(File file) throws IOException {
      if(file.getParentFile() != null){
          if(file.getParentFile().getParentFile() != null){
            if(!file.getParentFile().getParentFile().exists()){
              file.getParentFile().getParentFile().mkdirs();
            }
          }
          if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
          }
        }
        if(!file.exists()){
          file.createNewFile();
        }
    }
    
    private void transferDataToFile(Result result) {
      
      if(result != null && result.isSuccess()){
        User user = result.getUser();
        
        Map<String, Map<String, String>> workMap = user.getWorkHoursMap();
        XSSFWorkbook workBook = new XSSFWorkbook();
        XSSFSheet sheet = workBook.createSheet("MyTE Data");
        
        int rowNum = 0;
        int headCellCount = 1;
        Row statusRow = sheet.createRow(rowNum++); 
        Cell statusCell = statusRow.createCell(0);
        statusCell.setCellValue("Success Status:");
        
        Cell statusValCell = statusRow.createCell(1);
        statusValCell.setCellValue(result.getSuccessRatio().toString() + " %");
        
        Row monthRow = sheet.createRow(++rowNum);
        Cell monthCell = monthRow.createCell(0);
        monthCell.setCellValue("Processed Month:");
        
        
        
        Cell monthValCell = monthRow.createCell(1);
        monthValCell.setCellValue(new DateFormatSymbols().getMonths()[Integer.valueOf(user.getMonth())-1]);
        
        rowNum++;
        int headRowNo = ++rowNum;
        Row headRow = sheet.createRow(headRowNo);
        
        Cell headCell = headRow.createCell(0);
        headCell.setCellValue("Resource Name");
        
        Cell headIdCell = headRow.createCell(1);
        headIdCell.setCellValue("Resource Id");
        
        Map<String, Integer> taskMap = new HashMap<>();
        for(Map.Entry<String, Map< String, String>> workMapEntry : workMap.entrySet()){
          
          Row nextRow = sheet.createRow(++rowNum);
          
          String resourceKey = workMapEntry.getKey();
          
          String resourceName = resourceKey.split("\\^")[0];
          int cellNo = 0;
          Cell nameCell = nextRow.createCell(cellNo);
          nameCell.setCellValue(resourceName);
          
          String resourceId = resourceKey.split("\\^")[1];
          cellNo++;
          Cell idCell = nextRow.createCell(cellNo);
          idCell.setCellValue(resourceId);
          
          Map<String, String> hoursMap = workMapEntry.getValue(); 
          int headRowCellNo;
          for(Map.Entry<String, String> hoursEntry : hoursMap.entrySet()) {
            if(taskMap.containsKey(hoursEntry.getKey())) {
              headRowCellNo = taskMap.get(hoursEntry.getKey());
              
              Cell hoursValCell = nextRow.createCell(headRowCellNo);
              
              try{
                hoursValCell.setCellValue(new Double(hoursEntry.getValue()));
              } catch(Exception e) {
                e.printStackTrace();
                hoursValCell.setCellValue(hoursEntry.getValue());
              }
              
              
            }else {
              
              headCellCount++;
              taskMap.put(hoursEntry.getKey(), headCellCount);
              headRow.createCell(headCellCount).setCellValue(hoursEntry.getKey());
              try{
                nextRow.createCell(headCellCount).setCellValue(new Double(hoursEntry.getValue()));
              }catch(Exception e) {
                nextRow.createCell(headCellCount).setCellValue(hoursEntry.getValue());
              }
              
            }
            
          }
        }
        
        
        for(int colNo = 0; colNo <= headCellCount; colNo++) {
          sheet.autoSizeColumn(colNo, true);
        }
        
        try {
          Calendar cal = Calendar.getInstance();
          DateFormat formatter = new SimpleDateFormat("yyyy'-'MMM'-'dd'_'HH'_'mm'_'a");
          String fileName = "MyTEData" + "_" + formatter.format(cal.getTime()) + ".xlsx";
        FileOutputStream out = 
            new FileOutputStream(new File(fileName));
        workBook.write(out);
        out.close();
        workBook.close();
        
        } catch (FileNotFoundException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
        
        /**
         * Stores the user info into Preferences object
         * @param user
         */
        private void storeUserInfo(User user) {
        	if(prefs == null) {
          	  prefs = Preferences.userRoot().node(this.getClass().getName());
            }
			prefs.put(UserFields.DOMAIN.name(), user.getDomain());
			prefs.put(UserFields.USERNAME.name(), user.getUserName());
		}
        
        
        private User setUserInfo(User user) {
          
          user.setDomain(domainTextField.getText());
          user.setUserName(userTextField.getText());
          user.setPassword(pwBox.getText().toCharArray());
          user.setRole(group.getSelectedToggle().getUserData().toString());
          user.setMonth(combobox.getSelectionModel().getSelectedItem().getMonVal());
          user.setYear(now.get(Calendar.YEAR));
          return user;
          
        }
        
      });
      
      
      
      Platform.runLater(new Runnable() {
        @Override
        public void run() {
        	 if(userTextField.getText() == null || "".equalsIgnoreCase(userTextField.getText())) {
           	  	userTextField.requestFocus();
             } else{
           	  	pwBox.requestFocus();
             }
        }
      });
      
      
    }

	private GridPane getMainGrid(GridPane grid) {
		grid.setAlignment(Pos.CENTER);
		  grid.setHgap(10);
		  grid.setVgap(10);
		  grid.setPadding(new Insets(25, 25, 25, 25));
		  
		  return grid;
	}
    
    public void profileCleanUp() {
    	@SuppressWarnings("serial")
		List<File> dirList = new ArrayList<File>(){{
    		add(new File("profile"));
    	}};
    	
    	for(File dir : dirList){
    		purgeDirectory(dir);
    	}
    	
	}
    
    
    /**
     * Deletes all but one Cookies file in the custom profile of chrome
     * @param dir - Name of Directory/ folder whose childer files/folders will be recursively deleted.
     */
    void purgeDirectory(File dir) {
    	if(dir != null && dir.exists()){
    		for (File file: dir.listFiles()) {
                if (file.isDirectory()) purgeDirectory(file);
                if(!("chrome-test".equals(file.getAbsoluteFile().getParentFile().getParentFile().getName()) && "Cookies".equals(file.getName()))){
                	file.delete();
                }
                
            }
    	}
        
    }
    
    
    
    
 public static void main(String[] args) {
        launch(args);
    }
 	
 	@Override
 	public void stop() throws Exception{
 		profileCleanUp();
 		System.exit(0);
 	}
   public static class Month {
     private String monText;
     private String monVal;
  
     @Override
     public String toString() {
         return monText;
     }
  
     public Month(String monText, String monVal) {
         this.monText = monText;
         this.monVal = monVal;
     }
  
     public String getMonText() {
         return monText;
     }
  
     public void setMonText(String monText) {
         this.monText = monText;
     }
  
     public String getMonVal() {
         return monVal;
     }
  
     public void String(String monVal) {
         this.monVal = monVal;
     }
  }
}