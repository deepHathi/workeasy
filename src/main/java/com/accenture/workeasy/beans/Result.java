package com.accenture.workeasy.beans;

import java.util.List;

import com.accenture.workeasy.User;

public class Result{
	
	private boolean isSuccess;
	private User user;
	private Double successRatio;
	private boolean endedExceptionally;
	private List<Exception> exceptionList;
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Double getSuccessRatio() {
		return successRatio;
	}
	public void setSuccessRatio(Double successRatio) {
		this.successRatio = successRatio;
	}
	public boolean isEndedExceptionally() {
		return endedExceptionally;
	}
	public void setEndedExceptionally(boolean endedExceptionally) {
		this.endedExceptionally = endedExceptionally;
	}
	public List<Exception> getExceptionList() {
		return exceptionList;
	}
	public void setExceptionList(List<Exception> exceptionList) {
		this.exceptionList = exceptionList;
	}
}