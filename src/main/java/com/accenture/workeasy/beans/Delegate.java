package com.accenture.workeasy.beans;

public class Delegate {
  
  private String fullName;
  private String emailId;
  private Double totalHours;
  
  public String getFullName() {
    return fullName;
  }
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }
  public String getEmailId() {
    return emailId;
  }
  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }
  public Double getTotalHours() {
    return totalHours;
  }
  public void setTotalHours(Double totalHorus) {
    this.totalHours = totalHorus;
  }
}
