package com.accenture.workeasy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyTEAnalyser implements Callable<User> {
    
    private static Object firstHandle;
    private static Object lastHandle;
    private User user;
    
    public MyTEAnalyser(User user){
    	this.user = user;
    }
    
    public User analyse() throws Exception{
      
      
      setSystemProps();
      WebDriver driver = getWebDriver();
      
      try{
          driver.get("https://myte.accenture.com");
          return collectData(driver);
      }catch(Exception e){
    	  e.printStackTrace();
      }finally {
    	  if(driver != null){
    		  driver.quit();
    	  }
	}
      return user;
    }

    /**
     * Set necessary system properties. i.e. Set driver location for chrome/ firefox/ IE etc for different OS like windows, linux, OSX etc.
     * @throws Exception 
     */
    private void setSystemProps() throws Exception {
    	String driverPath = getDriverName();
    	if(driverPath == null) throw new Exception("OS Unknown");
    	File chromeDriver = getDriverFilePath(driverPath);
		System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
	}

	private String getDriverName() {
    	OsCheck.OSType ostype=OsCheck.getOperatingSystemType();
    	String driverPath = null;
    	switch (ostype) {
    	    case Windows:
    	    	driverPath = "execs/chromedriver.properties";
    	    	break;
    	    case MacOS: 
    	    	driverPath = "execs/chromedriver_mac";
    	    	break;
    	    case Linux:
    	    	boolean is64bit = System.getProperty("os.arch").indexOf("64") != -1; 
    	    	if(is64bit) {
    	    		driverPath = "execs/chromedriver_linux_64";
    	    	} else {
    	    		driverPath = "execs/chromedriver_linux_32";
    	    	}
    	    	break;
    	    case Other: 
    	    	break;
    	}
    	
    	return driverPath;
	}

	private File getDriverFilePath(String driverPath) throws IOException, FileNotFoundException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(driverPath);
		File chromeDriver = new File("profile/" + driverPath);
		createDirs(chromeDriver);
		FileOutputStream outputStream = new FileOutputStream(chromeDriver);
		IOUtils.copy(inputStream, outputStream);
		outputStream.flush();
		outputStream.close();
		if(!chromeDriver.canExecute()){
			chromeDriver.setExecutable(true);
		}
		return chromeDriver;
	}
    
    /**
     * 
     * @return WebDriver - return webdriver of the browser
     * @throws URISyntaxException 
     * @throws IOException 
     */
	private WebDriver getWebDriver() throws URISyntaxException, IOException {
    	ChromeOptions options = new ChromeOptions();
    	InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("execs/symantac.crx"); 
    	File file = new File("profile/execs/symantac.crx");
    	createDirs(file);
    	OutputStream outputStream = new FileOutputStream(file, false);
    	IOUtils.copy(inputStream, outputStream);
    	outputStream.flush();
    	outputStream.close();
    	options.addExtensions(file);
    	System.out.println(file.exists());
    	
    	File profileDir = new File("profile/chrome-test/");
    	if(!profileDir.getParentFile().exists()){
    		profileDir.mkdirs();
    	}
    	if(!profileDir.exists()){
    		profileDir.mkdir();
    	}
    	System.out.println(profileDir.getAbsolutePath());
        options.addArguments("--user-data-dir="+profileDir.getAbsolutePath());
        options.addArguments("--start-maximized");
        WebDriver driver = new ChromeDriver(options);
        return driver;
	}

	private void createDirs(File file) throws IOException {
		if(file.getParentFile() != null){
    		if(file.getParentFile().getParentFile() != null){
    			if(!file.getParentFile().getParentFile().exists()){
    				file.getParentFile().getParentFile().mkdirs();
    			}
    		}
    		if(!file.getParentFile().exists()){
    			file.getParentFile().mkdirs();
    		}
    	}
    	if(!file.exists()){
    		file.createNewFile();
    	}
	}

	private User collectData(WebDriver driver) throws InterruptedException {
		
		Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie cookie : cookies) {
			if(!cookie.getName().contains("_ia")) {
				driver.manage().deleteCookie(cookie);
			}
		}
		
		for(Cookie cookie : driver.manage().getCookies()) {
			System.out.println(cookie.getName() + " - Value - " + cookie.getValue());
		}
		
    	loginToMyTE(driver);
        
        /*HWND hwnd = User32.INSTANCE.FindWindow("MozillaDialogClass", "Security Warning");
        if(hwnd != null){
            User32.INSTANCE.PostMessage(hwnd, WinUser.WM_CLOSE, null, null);
            User32.INSTANCE.SetForegroundWindow(hwnd);
        }*/
        
        
        
        
        
        if (driver instanceof JavascriptExecutor)

        {
          switchToWindowsPopup(driver);
          WebDriverWait wait = new WebDriverWait(driver, 1*120);  
          wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ctl00_ctl00_PrimaryMenu_1")));
          
        
        boolean isSuccess = false;  
        ((JavascriptExecutor)driver).executeScript("__doPostBack('ctl00_ctl00_PrimaryMenu_1','Record');");
        {
          Thread.sleep(3000);	
          setUserDateStyle(driver, "ctl00_ctl00_MainContentPlaceHolder_tpcTimesheet_dropdownTimePeriod");
          
          Select select = new Select(driver.findElement(By.id("ctl00_ctl00_MainContentPlaceHolder_tpcTimesheet_dropdownTimePeriod")));
          isSuccess = selectDateOption(select, "15");
          
          Thread.sleep(5000);
        }
        
        Map<String, String> workHoursMap = new HashMap<String, String>();
        Double firstHalf = 0d;
        Double secondhalf = 0d;
        if(isSuccess) {
        	workHoursMap = getAllWorkHoursFortnight(driver, user, new HashMap<String, String>());
        	firstHalf = getWorkHourFortnight(driver, user);
        }

        
        {
          Thread.sleep(3000);	
          Select select = new Select(driver.findElement(By.id("ctl00_ctl00_MainContentPlaceHolder_tpcTimesheet_dropdownTimePeriod")));
          GregorianCalendar myCal = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Integer.parseInt(user.getMonth()) - 1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
          int maxDay = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);
          isSuccess = selectDateOption(select, String.valueOf(maxDay));
          
          Thread.sleep(5000);
        }
        
        if(isSuccess){
        	workHoursMap = getAllWorkHoursFortnight(driver, user, workHoursMap);
            secondhalf = getWorkHourFortnight(driver, user);
        }
        
        user.setWorkHoursMap(new HashMap<>());
        
        
        }
        
        driver.findElement(By.linkText("Log Out")).click();

        Thread.sleep(5000);
        
        if(driver != null){
      	  driver.quit();  
        }
        
        return user;
		
	}

	private boolean selectDateOption(Select select, String endDate) {
		boolean isSuccess = true;
		try{
			if(user.isUSStyleDate()){
	        	  select.selectByValue(user.getMonth()  + "/" + endDate + "/" + user.getYear());  
	          }else{
	        	  if(user.getMonth().length() == 1){
	        		  user.setMonth("0" + user.getMonth());
	        	  }
	        	  try{
	        		  select.selectByValue(endDate + "/" + user.getMonth() + "/" + user.getYear());  
	        	  }catch(Exception e){
	        		  isSuccess = false;
	        	  }
	        	    
	          }

		}catch(Exception e){
			isSuccess = false;
		}
		
		return isSuccess;
	}

	private void setUserDateStyle(WebDriver driver, String selectOption) {
		Select select = new Select(driver.findElement(By.id(selectOption)));
		String optionText = select.getFirstSelectedOption().getText();
	  	String optionStart = optionText.split("/")[0];
	  	if(optionStart.equals("15") || optionStart.equals("28") || optionStart.equals("29") || optionStart.equals("30") || optionStart.equals("31")){
	  		user.setUSStyleDate(false);
	  	}else{
	  		user.setUSStyleDate(true);
	  	}
	}

	private void loginToMyTE(WebDriver driver) {
		
		try{
	      	  WebDriverWait wait = new WebDriverWait(driver, 10);
	            
	            WebElement userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#userNameInput")));
	            
	                    
	            userName.sendKeys(user.getDomain()+ "\\" + user.getUserName());
	            Thread.sleep(1500);
	            WebElement password = driver.findElement(By.id("passwordInput"));
	            password.sendKeys(new String(user.getPassword()));
	            Thread.sleep(1500);
	            WebElement submit = driver.findElement(By.id("submitButton"));   
	            Thread.sleep(1500);
	            submit.click(); 
	              
	        }catch(Exception e){
	      	  
	        }
		
	}
	
	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}
	
	private static Map<String, String> getAllWorkHoursFortnight(WebDriver driver, User user, Map<String, String> workHoursMap){
		List<WebElement> rows = driver.findElements(By.xpath("//tr[@type='row']"));
		for(int rowCount = 0; rowCount < rows.size(); rowCount++) {
			WebElement row = rows.get(rowCount);
			WebElement wBSEle = row.findElement(By.xpath("./td[@idx='0']"));
			if(wBSEle.getText() != null && !"".equals(wBSEle.getText())){
				WebElement totalHours = getTotalHours(row);
				if(totalHours.getText() != null && !"".equals(totalHours.getText()) && isNumeric(totalHours.getText())){
					if(workHoursMap.containsKey(wBSEle.getText())){
						Double sumHours = Double.valueOf(workHoursMap.get(wBSEle.getText())) + Double.valueOf(totalHours.getText());
						workHoursMap.put(wBSEle.getText(), String.valueOf(sumHours));
					}else {
						workHoursMap.put(wBSEle.getText(), totalHours.getText());
					}
					
				}
			}
		}
		return workHoursMap;
	}
	
	
	private static Double getWorkHourFortnight(WebDriver driver, User user) {
      List<WebElement> billables= driver.findElements(By.xpath("//*[contains(text(), '" + user.getWbse() + "')]"));
      
      for(WebElement billEle : billables){
          if(!billEle.getText().equals("")){
        	  WebElement parent = billEle.findElement(By.xpath(".."));
        	  WebElement totalHours = null;
              totalHours = getTotalHours(parent);
      
              
              if(totalHours != null){
            	  return Double.valueOf(totalHours.getText());  
              } else {
            	  return 0d;  
              } 
          }
          
      }
      
      return 0d;
    }

	private static WebElement getTotalHours(WebElement parent) {
		  WebElement totalHours = null;
		  Boolean isPresentTotal = parent.findElements(By.className("TimesheetTotalHoursAssignmentsCell")).size() > 0;
		  Boolean isGridRowAdjusted = parent.findElements(By.className("gridRowAdjusted")).size() > 0;
		  if(isPresentTotal) {
			  totalHours = parent.findElement(By.className("TimesheetTotalHoursAssignmentsCell"));  
		  }else if(isGridRowAdjusted) {
			  boolean isEighteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='18']")).size() > 0;
			  boolean isSeventeen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='17']")).size() > 0;
			  boolean isSixteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='16']")).size() > 0;
			  boolean isFifteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='15']")).size() > 0;
			  if(isEighteen) {
				  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='18']"));
			  } else if(isSeventeen){
				  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='17']"));
			  } else if(isSixteen) {
				  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='16']"));
			  }  else if(isFifteen) {
				  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='15']"));
			  }
			  
		  }
		return totalHours;
	}

    public static void switchToWindowsPopup(WebDriver driver) {
      Set<String> handles = driver.getWindowHandles();
      Iterator<String> itr = handles.iterator();
      firstHandle = itr.next();
      lastHandle = firstHandle;
      while (itr.hasNext()) {
          lastHandle = itr.next();
      }
      driver.switchTo().window(lastHandle.toString());
    }



	@Override
	public User call() throws Exception {
		return analyse();
	}

}
