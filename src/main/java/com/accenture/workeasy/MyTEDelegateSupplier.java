package com.accenture.workeasy;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.accenture.workeasy.beans.Result;

public class MyTEDelegateSupplier implements Supplier<Result> {
	
	private static Object firstHandle;
    private static Object lastHandle;
    private User user;
    private Result result;
    private boolean isRetryDone;
    private WebDriver driver;
    
    public MyTEDelegateSupplier(User user, WebDriver driver){
    	this.user = user;
    	this.driver = driver;
    	this.result = new Result();
    	result.setUser(user);
    }
	@Override
	public Result get() {
		
		try {
			result = analyse();
		} catch (Exception e) {
			
			try{
				e.printStackTrace();
				result = getResultExceptionally(e);
			}catch(Exception eInternal){
				
			}
			
		}
		
		return result;
		
	}
	private Result getResultExceptionally(Exception e) {
		result.setSuccessRatio(0d);
		result.setEndedExceptionally(true);
		List<Exception> exceptionList = result.getExceptionList();
		if(exceptionList == null) {
			exceptionList = new ArrayList<Exception>();
		}
		exceptionList.add(e);
		result.setExceptionList(exceptionList);
		result.setSuccess(false);
		return result;
	}
	
	public Result analyse() throws Exception{
	      
	      try{
	          driver.get("https://myte.accenture.com");
	          return collectData(driver);
	      }catch(Exception e){
	        try{
	          e.printStackTrace();
	          result = getResultExceptionally(e);
	        }catch(Exception eInternal){
	          
	        }
	      }finally {
	    	  if(driver != null){
	    		  driver.quit();
	    	  }
		}
	      return new Result();
	    }

	    

		
	    
	    

		private Result collectData(WebDriver driver) throws InterruptedException {
			
			deleteCookies(driver);
			
	    	boolean flag = loginToMyTE(driver);
	        
	    	double overallRatio = 0;
	        if (driver instanceof JavascriptExecutor)

	        {
	          String ele = "Enter myTimeandExpenses";	
	          if(!flag){
	        	  ele = "Represent";
	          }
	          
	          WebDriverWait wait = new WebDriverWait(driver, 5*60);  
	          wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(ele)));
	          driver.findElement(By.linkText(ele)).click();
	          Thread.sleep(5000);
	        if(driver.findElements(By.linkText("Represent")).size() > 0) {
	          
	          Thread.sleep(500);	
	          driver.findElement(By.linkText("Represent")).click();
	          List<WebElement> delegateList = driver.findElements(By.xpath("//tr[@class='gridRowsDelegatees ']"));
	          double failNo = 0;
	          double totalNo = 0;
	          if(delegateList != null){
	        	  totalNo = delegateList.size();  
	          }
	          if(delegateList.size() > 0) {
	        	  Map<String, Map<String, String>> workHoursMap = new ConcurrentHashMap<>();
	        	  for(int delegateCount = 1; delegateCount <=  delegateList.size(); delegateCount++) {
	        		  try{

	        		    if(driver.findElements(By.xpath("//tr["+ delegateCount +"][@class='gridRowsDelegatees ']")).size() > 0){
	        		      WebElement delegateIdEle = null;
	        		      if(delegateCount == 1) {
	        		        delegateIdEle = driver.findElement(By.xpath("/html/body/div[@id='bodyInner']/form[@id='aspnetForm']/div[@id='content']/table[@id='ctl00_ctl00_MainContentPlaceHolder_TimeSheetTable']/tbody/tr[@id='ctl00_ctl00_MainContentPlaceHolder_tr_uc_Delegetees']/td[@id='ctl00_ctl00_MainContentPlaceHolder_tc_Delegetees']/div[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_DelegateesSelectorUpdatePanel']/div[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_pnl_DelegateeSelector']/table[@class='DelegateePanel TableWithCellPadding0Pixel']/tbody/tr[4]/td/div/div/table[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_grd_delegates']/tbody/tr[@class='gridRowsDelegatees ']["+ delegateCount +"]/td[5]"));
	        		      } else {
	        		        delegateIdEle = driver.findElement(By.xpath("/html/body/div[@id='bodyInner']/form[@id='aspnetForm']/div[@id='content']/table[@id='ctl00_ctl00_MainContentPlaceHolder_TimeSheetTable']/tbody/tr[@id='ctl00_ctl00_MainContentPlaceHolder_tr_uc_Delegetees']/td[@id='ctl00_ctl00_MainContentPlaceHolder_tc_Delegetees']/div[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_DelegateesSelectorUpdatePanel']/div[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_pnl_DelegateeSelector']/table[@class='DelegateePanel TableWithCellPadding0Pixel']/tbody/tr[4]/td/div/div/table[@id='ctl00_ctl00_MainContentPlaceHolder_uc_DelegeteesSelector_grd_delegates']/tbody/tr[@class='gridRowsDelegatees ']["+ (delegateCount - 1) +"]/td[5]"));
	        		      }
	        		      
                    String resourceId = delegateIdEle.getText();
	        		      
			        		  WebElement delegate = driver.findElement(By.xpath("//tr["+ delegateCount +"][@class='gridRowsDelegatees ']"));
			        		  delegate.click();
		        			  Thread.sleep(5000);

		        			  WebElement resourceNameSpan = driver.findElement(By.id("ctl00_ctl00_MainContentPlaceHolder_lnk_EmployeeNameText"));
		        			  String resourceName = resourceNameSpan.getText();
                    
                    String resourceKey = resourceName + "^" + resourceId;
		        			  
		        			  Map<String, String> resourceMap = new ConcurrentHashMap<>();
		        			  resourceMap = calculateWorkHours(driver);
		        			  workHoursMap.put(resourceKey, resourceMap);
		        		  }
	        		  }catch(Exception e) {
	        			  failNo++;
	        			  e.printStackTrace();
	        		  }
	        	  }
	        	  
	        	  if(totalNo != 0){
	        		  overallRatio = ((totalNo - failNo) * 100) / totalNo;
	        	  }
	        	  user.setWorkHoursMap(workHoursMap);
	          }
	        		  
	          
	        }
	        
	        
	        }
	        
	        if(driver.findElements(By.linkText("Log Out")).size() > 0){
	        	driver.findElement(By.linkText("Log Out")).click();
	        }else {
	        	if(isRetryDone == false){
	        		isRetryDone = true;
	        		collectData(driver);
	        	}
	        }
	        

	        Thread.sleep(5000);
	        
	        deleteCookies(driver);
	        
	        if(driver != null){
	      	  driver.quit();  
	        }
	        
	        if(driver != null){
		      	  driver.quit();  
		    }
	        
	        result.setUser(user);
	        result.setSuccess(true);
	        result.setEndedExceptionally(false);
	        result.setSuccessRatio(overallRatio);
	        
	        return result;
			
		}
		private Map<String, String> calculateWorkHours(WebDriver driver) throws InterruptedException {
				
			boolean isSuccess = setDateRange(driver, "15");
	          
	        Thread.sleep(5000);
	        
	        Map<String, String> workHoursMap = new ConcurrentHashMap<>();
	        if(isSuccess) {
	        	workHoursMap = getAllWorkHoursFortnight(driver, user, workHoursMap);
	        }
	        
	        
		    Thread.sleep(3000);	
		    isSuccess = setDateRange(driver, String.valueOf(getMaxDate()));
		    Thread.sleep(5000);
		    
		        
		        if(isSuccess){
		        	workHoursMap = getAllWorkHoursFortnight(driver, user, workHoursMap);
		        }
		        
		        return workHoursMap;
		        
			
		}
		private int getMaxDate() {
			GregorianCalendar myCal = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Integer.parseInt(user.getMonth()) - 1, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
			  int maxDay = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);
			return maxDay;
		}
		
		private boolean setDateRange(WebDriver driver, String endDate) {
			boolean isSuccess = false;;
			setUserDateStyle(driver, "ctl00_ctl00_MainContentPlaceHolder_tpcTimesheet_dropdownTimePeriod");
	          
	        Select select = new Select(driver.findElement(By.id("ctl00_ctl00_MainContentPlaceHolder_tpcTimesheet_dropdownTimePeriod")));
	        isSuccess = selectDateOption(select, endDate);
	        return isSuccess;
		}
		private void deleteCookies(WebDriver driver) {
			if(driver.manage() != null){
				Set<Cookie> cookies = driver.manage().getCookies();
				for(Cookie cookie : cookies) {
					if(!cookie.getName().contains("_ia01")) {
						driver.manage().deleteCookie(cookie);
					}
				}
				
				for(Cookie cookie : driver.manage().getCookies()) {
					System.out.println(cookie.getName() + " - Value - " + cookie.getValue());
				}
			}
			
		}

		private boolean selectDateOption(Select select, String endDate) {
			boolean isSuccess = true;
			try{
				if(user.isUSStyleDate()){
		        	  select.selectByValue(user.getMonth()  + "/" + endDate + "/" + user.getYear());  
		          }else{
		        	  if(user.getMonth().length() == 1){
		        		  user.setMonth("0" + user.getMonth());
		        	  }
		        	  try{
		        		  select.selectByValue(endDate + "/" + user.getMonth() + "/" + user.getYear());  
		        	  }catch(Exception e){
		        		  isSuccess = false;
		        	  }
		        	    
		          }

			}catch(Exception e){
				isSuccess = false;
			}
			
			return isSuccess;
		}

		private void setUserDateStyle(WebDriver driver, String selectOption) {
			Select select = new Select(driver.findElement(By.id(selectOption)));
			String optionText = select.getFirstSelectedOption().getText();
		  	String optionStart = optionText.split("/")[0];
		  	if(optionStart.equals("15") || optionStart.equals("28") || optionStart.equals("29") || optionStart.equals("30") || optionStart.equals("31")){
		  		user.setUSStyleDate(false);
		  	}else{
		  		user.setUSStyleDate(true);
		  	}
		}

		private boolean loginToMyTE(WebDriver driver) {
			boolean flag = true;
			try{
		      	  WebDriverWait wait = new WebDriverWait(driver, 10);
		            
		            WebElement userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#userNameInput")));
		            
		                    
		            userName.sendKeys(user.getDomain()+ "\\" + user.getUserName());
		            Thread.sleep(1500);
		            WebElement password = driver.findElement(By.id("passwordInput"));
		            password.sendKeys(new String(user.getPassword()));
		            Thread.sleep(1500);
		            WebElement submit = driver.findElement(By.id("submitButton"));   
		            Thread.sleep(1500);
		            submit.click(); 
		              
		        }catch(Exception e){
		      	  flag = false;
		        }
			return flag;
		}
		
		public static boolean isNumeric(String str)
		{
		  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
		}
		
		private static Map<String, String> getAllWorkHoursFortnight(WebDriver driver, User user, Map<String, String> workHoursMap){
      List<WebElement> rows = driver.findElements(By.xpath("//tr[@type='row']"));
      for(int rowCount = 0; rowCount < rows.size(); rowCount++) {
        WebElement row = rows.get(rowCount);
        WebElement wBSEle = row.findElement(By.xpath("./td[@idx='0']"));
        if(wBSEle.getText() != null && !"".equals(wBSEle.getText())){
          WebElement totalHours = getTotalHours(row);
          if(totalHours.getText() != null && !"".equals(totalHours.getText()) && isNumeric(totalHours.getText())){
            if(workHoursMap.containsKey(wBSEle.getText())){
              Double sumHours = Double.valueOf(workHoursMap.get(wBSEle.getText())) + Double.valueOf(totalHours.getText());
              workHoursMap.put(wBSEle.getText(), String.valueOf(sumHours));
            }else {
              workHoursMap.put(wBSEle.getText(), totalHours.getText());
            }
            
          }
        }
      }
      return workHoursMap;
    }
		
		private static WebElement getTotalHours(WebElement parent) {
			  WebElement totalHours = null;
			  Boolean isPresentTotal = parent.findElements(By.className("TimesheetTotalHoursAssignmentsCell")).size() > 0;
			  Boolean isGridRowAdjusted = parent.findElements(By.className("gridRowAdjusted")).size() > 0;
			  if(isPresentTotal) {
				  totalHours = parent.findElement(By.className("TimesheetTotalHoursAssignmentsCell"));  
			  }else if(isGridRowAdjusted) {
				  boolean isEighteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='18']")).size() > 0;
				  boolean isSeventeen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='17']")).size() > 0;
				  boolean isSixteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='16']")).size() > 0;
				  boolean isFifteen = parent.findElements(By.cssSelector(".gridRowAdjusted[idx='15']")).size() > 0;
				  if(isEighteen) {
					  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='18']"));
				  } else if(isSeventeen){
					  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='17']"));
				  } else if(isSixteen) {
					  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='16']"));
				  }  else if(isFifteen) {
					  totalHours = parent.findElement(By.cssSelector(".gridRowAdjusted[idx='15']"));
				  }
				  
			  }
			return totalHours;
		}

	    public static void switchToWindowsPopup(WebDriver driver) {
	      Set<String> handles = driver.getWindowHandles();
	      Iterator<String> itr = handles.iterator();
	      firstHandle = itr.next();
	      lastHandle = firstHandle;
	      while (itr.hasNext()) {
	          lastHandle = itr.next();
	      }
	      driver.switchTo().window(lastHandle.toString());
	    }




}
